﻿using MySql.Data.MySqlClient;
using Nsf._2018.Modulo2.DB.Prova.DB.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.Modulo2.DB.Prova.DB.Resistencia
{
    class ResistenciaDatabase
    {
        public int Salvar(ResistenciaDTO dto)
        {
            string script =
                @"insert into tb_resistencia (nm_pessoa, ds_mensagem)
	                                  values (@nm_pessoa, @ds_mensagem)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_pessoa", dto.Nome));
            parms.Add(new MySqlParameter("ds_mensagem", dto.Mensagem));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }

        public List<ResistenciaDTO> Listar()
        {
            string script = "select * from tb_resistencia";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);


            List<ResistenciaDTO> lista = new List<ResistenciaDTO>();
            while (reader.Read())
            {
                ResistenciaDTO dto = new ResistenciaDTO();
                dto.Id = reader.GetInt32("id_resistencia");
                dto.Nome = reader.GetString("nm_pessoa");
                dto.Mensagem = reader.GetString("ds_mensagem");

                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }

    }
}

